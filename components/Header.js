// base imports
import Head from 'next/head'
import Link from 'next/link'
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap'
import { useContext } from 'react';
import UserContext from '../UserContext'

const Header = () => {
    const { user } = useContext(UserContext);
    // console.log(user)

    return <>
    <Head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    </Head> 
    
    <header>
        <Navbar bg="dark" variant="dark" expand="lg" collapseOnSelect>
            <Container>
                <Link href="/">
			        <a className="navbar-brand"><i className="fas fa-money-bill"></i> Budget Tracker</a>
		        </Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        {/* <Link href="/">
                            <a className="nav-link" role="button">Home</a>
                        </Link> */}
                        {
                            (user.id !== null) ?
                            <>  
                                {/* put drowndown */}
                                        <Link href="/charts/balance-trend">
                                            <a className="nav-link" role="button"><i className="fas fa-"></i> Trend</a>
                                        </Link>
                                        <Link href="/charts/expense-breakdown">
                                            <a className="nav-link" role="button"><i className="fas fa-"></i> Expense</a>
                                        </Link>
                                    
                                        <Link href="/charts/income-breakdown">
                                            <a className="nav-link" role="button"><i className="fas fa-"></i> Income</a>
                                        </Link>
                                        <Link href="/charts/category-breakdown">
                                            <a className="nav-link" role="button"><i className="fas fa-"></i> Breakdown</a>
                                        </Link>

        
                                <Link href="/records">
                                    <a className="nav-link" role="button"><i className="fas fa-"></i> Records</a>
                                </Link>
                                <Link href="/categories">
                                    <a className="nav-link" role="button"><i className="fas fa-"></i> Categories</a>
                                </Link>
                                <Link href="/logout">
                                    <a className="nav-link" role="button"><i className="fas fa-user"></i> Log Out</a>
                                </Link>
                            </>
                            :
                            <>
                                <Link href="/register">
                                    <a className="nav-link" role="button"><i className="fas fa-user-plus"></i> Sign Up</a>
                                </Link>
                                <Link href="/login">
                                    <a className="nav-link" role="button"><i className="fas fa-user"></i> Sign In</a>
                                </Link>
                            </>   
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    </header>
    </>
}

export default Header

